package com.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CustomProcessor implements Processor{
    //System.out.println("Hello");
	
	/*public void process(Exchange exchange) throws Exception {
        String originalFileContent = (String) exchange.getIn().getBody(String.class);
        String upperCaseFileContent = originalFileContent.toUpperCase();
        exchange.getIn().setBody(upperCaseFileContent);
    }*/
	
	public void process(Exchange exc)throws Exception{
		
		String originalFileContentInLowercase = (String) exc.getIn().getBody(String.class);
		String upperCaseFileContent = originalFileContentInLowercase.toUpperCase();
	//	String originalFileContentInLowercase = (String) exc.getIn().getBody(String.class);
	//	String upperCaseFileContent = originalFileContentInLowercase.toUpperCase();

		exc.getIn().setBody(upperCaseFileContent);
	}

}
